%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.
%
%  DESCRIPTION:
%
% Node local peridynamics is a simple 2D peridynamics code
% for regular affine grids that uses the standard FEM stencil
% as the horizon. This local restriction of the horizon leads
% to a formulation equivalent to classical solid mechanics, but
% is integral based and naturally incorporates discontinuities.

function [ displacement bc_flags ] = boundary_conditions( type, initial_coords, ...
    displacement, num_nodes_x,num_nodes_y)

num_nodes = num_nodes_x * num_nodes_y;
bc_flags = zeros(num_nodes*2,1);

switch type
    case 'unit_dilat'
        e0 = 0.001;
        displacement = initial_coords.*e0;
        bc_flags(:) = 1;
    case 'unit_shear'
        gamma = 0.001;
        displacement = [(initial_coords(:,2).*gamma) ...
                         (initial_coords(:,1).*gamma)];
        bc_flags(:) = 1;
    case 'simple_compression_y'
        delta = 0.001;
        top_start = (num_nodes_y-1) * num_nodes_x;
        bottom_start = 0;
        for i=1:num_nodes_x
            % displace the top edge
            displacement(top_start + i,2) = - delta;
            flag_index = (top_start + i - 1) * 2 + 2;
            bc_flags(flag_index) = 1;
            % bottom edge is no displacement
            displacement(bottom_start + i,2) = 0;
            flag_index = (bottom_start + i - 1) * 2 + 2;
            bc_flags(flag_index) = 1;
            
            % displace the top edge x = 0
            displacement(top_start + i,1) = 0;
            flag_index = (top_start + i - 1) * 2 + 1;
            bc_flags(flag_index) = 1;
            % bottom edge is no displacement
            displacement(bottom_start + i,1) = 0;
            flag_index = (bottom_start + i - 1) * 2 + 1;
            bc_flags(flag_index) = 1;
            
            
        end
        % x disp in the corner is zero
        %displacement(1,1) = 0;
        %bc_flags(1) = 1;
        % zero x disp for left and right side right
        for i=1:num_nodes_y
            stride = (i-1)*num_nodes_x;
            displacement(stride+1,1) = 0;
            displacement(stride + num_nodes_x,1) = 0;
            flag_index = (stride)*2 + 1;
            bc_flags(flag_index) = 1;
            flag_index = (stride + num_nodes_x - 1) * 2 + 1;
            bc_flags(flag_index) = 1;
        end
    case 'adjoint_pull'
        delta = 0.001;
        top_start = (num_nodes_y-1) * num_nodes_x;
        bottom_start = 0;
        for i=1:num_nodes_x
            % displace the top edge
            displacement(top_start + i,1) = + delta;
            flag_index = (top_start + i - 1) * 2 + 1;
            bc_flags(flag_index) = 1;
            displacement(top_start + i,2) = + delta;
            flag_index = (top_start + i - 1) * 2 + 2;
            bc_flags(flag_index) = 1;
            % bottom edge is no displacement
            displacement(bottom_start + i,2) = 0;
            flag_index = (bottom_start + i - 1) * 2 + 2;
            bc_flags(flag_index) = 1;
        end
        % x disp in the corner is zero
        displacement(1,1) = 0;
        bc_flags(1) = 1;
    case 'adjoint_pull_force'
        %delta = 0.001;
        %top_start = (num_nodes_y-1) * num_nodes_x;
        bottom_start = 0;
        for i=1:num_nodes_x
            % displace the top edge
            %displacement(top_start + i,1) = + delta;
            %flag_index = (top_start + i - 1) * 2 + 1;
            %bc_flags(flag_index) = 1;
            %displacement(top_start + i,2) = + delta;
            %flag_index = (top_start + i - 1) * 2 + 2;
            %bc_flags(flag_index) = 1;
            % bottom edge is no displacement
            displacement(bottom_start + i,2) = 0;
            flag_index = (bottom_start + i - 1) * 2 + 2;
            bc_flags(flag_index) = 1;
        end
        % x disp in the corner is zero
        displacement(1,1) = 0;
        bc_flags(1) = 1;
    case 'force_compression_y'
        top_start = (num_nodes_y-1) * num_nodes_x;
        bottom_start = 0;
        for i=1:num_nodes_x
            % bottom edge is no y displacement
            displacement(bottom_start + i,2) = 0;
            flag_index = (bottom_start + i - 1) * 2 + 2;
            bc_flags(flag_index) = 1;
            % bottom edge is no x displacement
            displacement(bottom_start + i,1) = 0;
            flag_index = (bottom_start + i - 1) * 2 + 1;
            bc_flags(flag_index) = 1;
        end
        %for i=1:num_nodes_y
        %    stride = (i-1)*num_nodes_x;
        %    displacement(stride+1,1) = 0;
        %    displacement(stride + num_nodes_x,1) = 0;
        %    flag_index = (stride)*2 + 1;
        %    bc_flags(flag_index) = 1;
        %    flag_index = (stride + num_nodes_x - 1) * 2 + 1;
        %   bc_flags(flag_index) = 1;
        %end
    case 'boundary_dilat'
        delta = 0.001;
        % move bottom down and top up
        top_start = (num_nodes_y-1)*num_nodes_x;
        for i=1:num_nodes_x
            displacement(i,2) = -delta;
            flag_index = (i-1)*2 + 2;
            bc_flags(flag_index) = 1;
            displacement(top_start + i,2) = delta;
            flag_index = (top_start + i - 1) * 2 + 2;
            bc_flags(flag_index) = 1;
        end
        % move left side left and right side right
        for i=1:num_nodes_y
            stride = (i-1)*num_nodes_x;
            displacement(stride+1,1) = -delta;
            displacement(stride + num_nodes_x,1) = delta;
            flag_index = (stride)*2 + 1;
            bc_flags(flag_index) = 1;
            flag_index = (stride + num_nodes_x - 1) * 2 + 1;
            bc_flags(flag_index) = 1;
        end
    case 'lid_driven_cavity'
        delta = 0.0001;
        top_start = (num_nodes_y-1) * num_nodes_x;
        bottom_start = 0;
        for i=1:num_nodes_x
            % displace the top edge
            displacement(top_start + i,1) = delta;
            flag_index = (top_start + i - 1) * 2 + 1;
            bc_flags(flag_index) = 1;
            displacement(top_start + i,2) = 0;
            flag_index = (top_start + i - 1) * 2 + 2;
            bc_flags(flag_index) = 1;
            % bottom edge is no displacement
            displacement(bottom_start + i,1) = 0;
            flag_index = (bottom_start + i - 1) * 2 + 1;
            bc_flags(flag_index) = 1;
            displacement(bottom_start + i,2) = 0;
            flag_index = (bottom_start + i - 1) * 2 + 2;
            bc_flags(flag_index) = 1;
        end
        % zero x disp for left and right side right
        for i=1:num_nodes_y
            stride = (i-1)*num_nodes_x;
            displacement(stride+1,1) = 0;
            displacement(stride + num_nodes_x,1) = 0;
            flag_index = (stride)*2 + 1;
            bc_flags(flag_index) = 1;
            flag_index = (stride + num_nodes_x - 1) * 2 + 1;
            bc_flags(flag_index) = 1;
            displacement(stride+1,2) = 0;
            displacement(stride + num_nodes_x,2) = 0;
            flag_index = (stride)*2 + 2;
            bc_flags(flag_index) = 1;
            flag_index = (stride + num_nodes_x - 1) * 2 + 2;
            bc_flags(flag_index) = 1;
        end
        
    otherwise
        error(sprintf('\n** UNKOWN BOUNDARY CONDITION **\n'));    
end


end