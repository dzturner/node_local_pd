%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.
%
%  DESCRIPTION:
%
% Node local peridynamics is a simple 2D peridynamics code
% for regular affine grids that uses the standard FEM stencil
% as the horizon. This local restriction of the horizon leads
% to a formulation equivalent to classical solid mechanics, but
% is integral based and naturally incorporates discontinuities.

clear; clc;

disp('...                               ...');
disp('... Node-based local peridynamics ...');
disp('...                               ...');
disp('');

% read in the displacement data
u_star = dlmread('Forward_Disp_Constant_Small.txt');

force_mag = 1000.0;

E_0  = 1000.0; % How's it putting in E and nu for a change, Callie?:)
nu_0 = 0.25;

% number of iterations
max_its = 10;
tol = 1E-5;

epsilon = 1E-3;

% square domain
length = 1000.0;
% length must be divisable by h
h = length / 2.0;
%length + h;
num_elem_x = length / h;
num_elem_y = length / h;
num_elem = num_elem_x * num_elem_y;
num_nodes_x = num_elem_x + 1;
num_nodes_y = num_elem_y + 1;
num_nodes = num_nodes_x * num_nodes_y;

% create the sensetivity stencils:
[ node_stencil, node_sens ] = node_stencil(num_nodes_x, num_nodes_y);

% create the node grid coords
initial_coords = coords(num_nodes_x, num_nodes_y, h);
current_coords = initial_coords;

% converted material properties:
r = zeros(num_nodes,1);
for i = 1:num_nodes
    r(i) = initial_coords(i,1)*initial_coords(i,1) + ...
        initial_coords(i,2)*initial_coords(i,2);
end
r = sqrt(r);
K_0  = E_0 / (3*(1-2*nu_0));
mu_0 = E_0 / (2*(1+nu_0));

%factor_k = 0.00025;
%factor_mu = 0.0002;
%K = -factor_k.*(r).*K_0 + K_0;
%mu = factor_mu.*(r).*mu_0 + mu_0;

%K = K.*0.5;
%mu = mu.*0.5;

K = zeros(num_nodes,1);
K(:) = K_0;
mu = zeros(num_nodes,1);
mu(:) = mu_0;


% generate the list of straight bonds and diagonal bonds
[ bonds, num_bonds, ...
  i_bonds, num_i_bonds, ...
  d_bonds, num_d_bonds] = bonds(num_nodes_x, num_nodes_y);

% compute the cell sizes
cell_size = cell_size(num_nodes_x, num_nodes_y, h);

% compute the weighted size
[ weighted_size, ...
  dilat_weighted_size, ...
  dev_weighted_size ] = weighted_size(num_nodes_x, num_nodes_y, ...
                            i_bonds, d_bonds, initial_coords, cell_size);
 
% initialize displacement field
displacement = zeros(num_nodes,2);

% allocate the tangent matrix
tangent = zeros(num_nodes*2,num_nodes*2);

% allocate the reaction vector
internal_force = zeros(num_nodes*2,1);

for iter = 1:max_its 

    % boundary conditions
    [ displacement, bc_flags ] = boundary_conditions('adjoint_pull_force', ...
         initial_coords, displacement, num_nodes_x, num_nodes_y);
    %displacement
     
    current_coords = initial_coords + displacement;

    % compute the dilataion
    it_dilat =  dilat(num_nodes, num_bonds, bonds, initial_coords, ...
    current_coords, weighted_size, cell_size);
    
    %compute the residual
    it_residual = zeros(num_nodes*2,1);
    %RHS = zeros(num_nodes*2,1);
    %it_stab = zeros(num_nodes,1);
    for node=1:num_nodes
        for dim=1:2
            resid_index = (node-1)*2 + dim;
            it_residual(resid_index) = residual_adjoint(node,dim,node_stencil,...
                initial_coords, current_coords, cell_size, weighted_size, it_dilat,...
                K, mu);
        end
    end
    %it_residual
    internal_force = it_residual;
    
    % apply bcs to residual    
    for i=1:num_nodes*2
        if(bc_flags(i)==1)
            it_residual(i)  = 0.0;
        end
    end
    %it_residual
    
    %plot_quantity(it_residual,5,length,length,num_nodes_x,num_nodes_y,h); 
    
    % add the external force
    force_it = 0;
    stride_start = (num_nodes_y-1)*num_nodes_x*2;
    for n=1:num_nodes_x
        force_it = force_it + 2; % only hit the y-comps
        force_val = force_mag/(num_nodes_x-1);
        if n==1 || n==num_nodes_x
            force_val = force_val/2;
       end
        it_residual(stride_start + force_it-1) = it_residual(stride_start + force_it-1)...
           + force_val;
        it_residual(stride_start + force_it) = it_residual(stride_start + force_it)...
           + force_val;
    end
    %it_residual
    
    %plot_quantity(it_residual,5,length,length,num_nodes_x,num_nodes_y,h); 

    % test the residual norm
    fprintf('\nIt: %i Residual: %f\n\n', iter,norm(it_residual))
    % test the residual norm
    if(norm(it_residual) < tol)
        fprintf('\n-----------------\n Step Converged!\n-----------------\n'); 
        break;
    end
    if(iter==max_its+1)
        fprintf('\n-----------------\n Step Failed!\n-----------------\n');
        %
        %error();
    end
    
    % only compute the tangent on the first iteration
    if (iter==1)
        fprintf('STATUS: Computing the tangent...\n')
        % initialize temp arrays
        coords_plus  = current_coords;
        coords_minus = current_coords;
        dilat_plus   = zeros(num_nodes,1);
        dilat_minus  = zeros(num_nodes,1);
        
        num_sens = 25;
        for node=1:num_nodes
            for dim=1:2
                row = (node-1)*2 + dim;
                for sens = 1:num_sens
                    pert_node = node_sens(node,sens);
                    if(pert_node==0);continue; end
                    for pert_dim = 1:2
                        col = (pert_node-1)*2 + pert_dim;
                        % perturb the coordinates
                        coords_plus(pert_node,pert_dim) = ...
                            coords_plus(pert_node,pert_dim) + epsilon;
                        coords_minus(pert_node,pert_dim) = ...
                            coords_minus(pert_node,pert_dim) - epsilon;
                        
                        % compute the perturbed dilats
                        dilat_plus = dilat(num_nodes, num_bonds, bonds, initial_coords, ...
                            coords_plus, weighted_size, cell_size);
                        dilat_minus = dilat(num_nodes, num_bonds, bonds, initial_coords, ...
                            coords_minus, weighted_size, cell_size);
                        
                        % calculate the tangent contribs
                         resid_plus = residual_adjoint(node,dim,node_stencil,initial_coords,...
                             coords_plus, cell_size, weighted_size, dilat_plus, K, mu);
                         resid_minus = residual_adjoint(node,dim,node_stencil,initial_coords,...
                             coords_minus, cell_size, weighted_size, dilat_minus, K, mu);
                        
                        value = (resid_plus - resid_minus)/(2*epsilon);
                        
                        tangent(row,col) = tangent(row,col) + value;
                        
                        % reset the coordinates
                        coords_plus(pert_node,pert_dim) = ...
                            coords_plus(pert_node,pert_dim) - epsilon;
                        coords_minus(pert_node,pert_dim) = ...
                            coords_minus(pert_node,pert_dim) + epsilon;
                    end
                end
            end
        end
        %tangent
        
         % apply bcs to the tangent
         for i=1:num_nodes*2
             if(bc_flags(i)==1)
                 tangent(i,:) = 0;
                 tangent(:,i) = 0;
                 tangent(i,i) = 1.0/epsilon;
                 % multiply by the stiffness to scale better
             end
         end
        %tangent
        fprintf('STATUS: Done computing the tangent\n')
    end
    
    fprintf('STATUS: Solving...\n')
    %lhs = inv(tangent) * (-it_residual);
    lhs = tangent \ -it_residual;
    fprintf('STATUS: Done solving\n')
    
    % update the displacments:
    % % update coords
    for node=1:num_nodes
        for dim=1:2
            index = (node-1)*2 + dim;
            displacement(node,dim) = displacement(node,dim) + lhs(index);
        end
    end
     
end


output_vec = zeros(num_nodes*2,1);
for node=1:num_nodes
    for dim=1:2
        index = (node-1)*2 + dim;
        output_vec(index) = displacement(node,dim);
    end
end
%plot_quantity(K,1,length,length,num_nodes_x,num_nodes_y,h); 
%plot_quantity(mu,1,length,length,num_nodes_x,num_nodes_y,h); 
plot_quantity(displacement,5,length,length,num_nodes_x,num_nodes_y,h); 

max(output_vec - u_star)

% write out the displacments
dlmwrite('Forward_Disp_Constant_Small.txt',output_vec,'precision',12)

%scalar = objective(lhs);


%disp(' ');
%disp('Force reaction at boundary:');
%total_reaction = reaction(2,num_nodes_x, num_nodes_y, internal_force)

% % calculate the misfit:
% g = 0.0; % x-component
% %g = 0.0; % y-component
% for i=1:num_nodes
%     u_m_u0 = displacement(i,:) - u_obs(i,:);
%     u_m_u0_mag = sqrt(u_m_u0(1)^2 + u_m_u0(2)^2);
%     g = g + (0.5*K_0*mu_0)*(u_m_u0_mag)^2;
% end
% fprintf('MISFIT: g(x) = %f \n\n',g);
% 
% % compute g_x
% g_x = zeros(num_nodes*2,1);
% for i=1:num_nodes
%     for dim=1:2
%         stride = (i-1)*2 + dim;
%         g_x(stride) = (K_0*mu_0)*(displacement(i,dim) - u_obs(i,dim));
%     end
% end
% 
% % solve for lambda:
% lambda = tangent' \ g_x;
% 
% % compute A_p
% Apx = zeros(num_nodes*2,num_nodes);
% 
% K_plus = K;
% K_minus = K;
% for i=1:num_nodes
%     % perturn the K values
%     K_plus(i) = K_plus(i) + epsilon;
%     K_minus(i) = K_minus(i) - epsilon;
% 
%     % compute tangent plus and minus
%     tangent_plus  = zeros(num_nodes*2,num_nodes*2);
%     tangent_minus = zeros(num_nodes*2,num_nodes*2);
%     tangent_pi    = zeros(num_nodes*2,num_nodes*2);
%     
%     fprintf('STATUS: Computing Api %f...\n',i)
%     % initialize temp arrays
%     coords_plus  = current_coords;
%     coords_minus = current_coords;
%     dilat_plus   = zeros(num_nodes,1);
%     dilat_minus  = zeros(num_nodes,1);
%     
%     num_sens = 25;
%     for node=1:num_nodes
%         for dim=1:2
%             row = (node-1)*2 + dim;
%             for sens = 1:num_sens
%                 pert_node = node_sens(node,sens);
%                 if(pert_node==0);continue; end
%                 for pert_dim = 1:2
%                     col = (pert_node-1)*2 + pert_dim;
%                     % perturb the coordinates
%                     coords_plus(pert_node,pert_dim) = ...
%                         coords_plus(pert_node,pert_dim) + epsilon;
%                     coords_minus(pert_node,pert_dim) = ...
%                         coords_minus(pert_node,pert_dim) - epsilon;
%                     
%                     % compute the perturbed dilats
%                     dilat_plus = dilat(num_nodes, num_bonds, bonds, initial_coords, ...
%                         coords_plus, weighted_size, cell_size);
%                     dilat_minus = dilat(num_nodes, num_bonds, bonds, initial_coords, ...
%                         coords_minus, weighted_size, cell_size);
%                     
%                     % calculate the tangent contribs
%                     resid_plus_kp = residual_adjoint(node,dim,node_stencil,initial_coords,...
%                         coords_plus, cell_size, weighted_size, dilat_plus, K_plus, mu);
%                     resid_minus_kp = residual_adjoint(node,dim,node_stencil,initial_coords,...
%                         coords_minus, cell_size, weighted_size, dilat_minus, K_plus, mu);
%                     resid_plus_km = residual_adjoint(node,dim,node_stencil,initial_coords,...
%                         coords_plus, cell_size, weighted_size, dilat_plus, K_minus, mu);
%                     resid_minus_km = residual_adjoint(node,dim,node_stencil,initial_coords,...
%                         coords_minus, cell_size, weighted_size, dilat_minus, K_minus, mu);
%                     
%                     value_kp = (resid_plus_kp - resid_minus_kp)/(2*epsilon);
%                     value_km = (resid_plus_km - resid_minus_km)/(2*epsilon);
%                     
%                     tangent_plus(row,col)  = tangent_plus(row,col)  + value_kp;
%                     tangent_minus(row,col) = tangent_minus(row,col) + value_km;
%                     
%                     % reset the coordinates
%                     coords_plus(pert_node,pert_dim) = ...
%                         coords_plus(pert_node,pert_dim) - epsilon;
%                     coords_minus(pert_node,pert_dim) = ...
%                         coords_minus(pert_node,pert_dim) + epsilon;
%                 end
%             end
%         end
%     end
%       
%     %tangent_pi
%     for m=1:num_nodes*2
%         for n=1:num_nodes*2
%             tangent_pi(m,n) = (tangent_plus(m,n) - tangent_minus(m,n))/(2*epsilon);
%         end
%     end
%     
%             % apply bcs to the tangent
%     for m=1:num_nodes*2
%         if(bc_flags(m)==1)
%             tangent_pi(m,:) = 0;
%             tangent_pi(:,m) = 0;
%             tangent_pi(m,m) = 1.0/epsilon;
%             % multiply by the stiffness to scale better
%         end
%     end
%     Apx(:,i) = tangent_pi * adj_x;
%     
%     
%     
%     
%     %tangent
%     %fprintf('STATUS: Done computing the Api: %f\n',i)
%     
% 
%     % restore the K values
%     K_plus(i) = K_plus(i) - epsilon;
%     K_minus(i) = K_minus(i) + epsilon;
% end
% %Apx
% 
% dg_dp = -lambda'*Apx
% size(dg_dp)

%plot_quantity(displacement - u_obs,5,length,length,num_nodes_x,num_nodes_y,h); 
%plot_quantity(K,0,length,length,num_nodes_x,num_nodes_y,h); 
%plot_quantity(it_dilat,0,length,length,num_nodes_x,num_nodes_y,h); 
%plot_quantity(mu,0,length,length,num_nodes_x,num_nodes_y,h); 
%plot_quantity(displacement,1,length,length,num_nodes_x,num_nodes_y,h); 
%plot_quantity(it_stab,5,length,length,num_nodes_x,num_nodes_y,h); 


% displacement
% construct the x vector
%adj_x = zeros(num_nodes*2,1);
%for i=1:num_nodes
%    for dim=1:2
%        stride = (i-1)*2 + dim;
%        adj_x(stride) = displacement(i,dim);
%    end
%end
% write out the displacments
%dlmwrite('Forward_Disp.txt',adj_x)


 