%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [ tangent ] = tangent_multigrid(K, mu, h, epsilon,...
    num_nodes, num_elem, ...
    local_bonds, connectivity, initial_coords, current_coords)


tangent = zeros(num_nodes*2,num_nodes*2);

num_local_bonds       = length(local_bonds(:,1));

m     = 8*h^4;
s     = h^4;
kappa = K + mu/3.0;
alpha = 8.0*mu/m;

for i = 1:num_elem

    % construct an element set of coordinates:
    %elem_initial_coords = zeros(4,2);
    elem_current_coords = zeros(4,2);
    for(node=1:4)
        %elem_initial_coords(node,1) = initial_coords(connectivity(i,node),1);
        %elem_initial_coords(node,2) = initial_coords(connectivity(i,node),2);
        elem_current_coords(node,1) = current_coords(connectivity(i,node),1);
        elem_current_coords(node,2) = current_coords(connectivity(i,node),2);
    end
    %elem_initial_coords
    %elem_current_coords
    
    for pert_node = 1:4
       for dim=1:2 
           tangent_col = (connectivity(i,pert_node)-1)*2 + dim;
           
           plus_elem_current_coords = elem_current_coords;
           minus_elem_current_coords = elem_current_coords;
           plus_elem_current_coords(pert_node,dim) = plus_elem_current_coords(pert_node,dim) ...
               + epsilon;
           minus_elem_current_coords(pert_node,dim) = minus_elem_current_coords(pert_node,dim) ...
               - epsilon;
           
           % compute dilat plus and dilat minus    
           dilat_plus = 0.0;
           dilat_minus = 0.0;
           for bond = 1:num_local_bonds
               
               local_left = local_bonds(bond,1);
               local_right = local_bonds(bond,2);
               left_node = connectivity(i,local_left);
               right_node = connectivity(i,local_right);
               
               dx = initial_coords(right_node,1) - initial_coords(left_node,1);
               dy = initial_coords(right_node,2) - initial_coords(left_node,2);
               
               DX_plus = plus_elem_current_coords(local_right,1) - plus_elem_current_coords(local_left,1);
               DY_plus = plus_elem_current_coords(local_right,2) - plus_elem_current_coords(local_left,2);
               DX_minus = minus_elem_current_coords(local_right,1) - minus_elem_current_coords(local_left,1);
               DY_minus = minus_elem_current_coords(local_right,2) - minus_elem_current_coords(local_left,2);
               
               x_bar = sqrt(dx^2 + dy^2);
               
               y_bar_plus  = sqrt(DX_plus^2 + DY_plus^2);
               y_bar_minus = sqrt(DX_minus^2 + DY_minus^2);

               dilat_plus  = dilat_plus + 2.0 * (y_bar_plus - x_bar) * x_bar * s / m;
               dilat_minus = dilat_minus + 2.0 * (y_bar_minus - x_bar) * x_bar * s / m;
               
           end
           %dilat_dual(i)
           %dilat_plus
           %dilat_minus
           
           for bond = 1:num_local_bonds
               
               local_left = local_bonds(bond,1);
               local_right = local_bonds(bond,2);
               left_node = connectivity(i,local_left);
               right_node = connectivity(i,local_right);
               
               dx = initial_coords(right_node,1) - initial_coords(left_node,1);
               dy = initial_coords(right_node,2) - initial_coords(left_node,2);
               
               DX_plus = plus_elem_current_coords(local_right,1) - plus_elem_current_coords(local_left,1);
               DY_plus = plus_elem_current_coords(local_right,2) - plus_elem_current_coords(local_left,2);
               DX_minus = minus_elem_current_coords(local_right,1) - minus_elem_current_coords(local_left,1);
               DY_minus = minus_elem_current_coords(local_right,2) - minus_elem_current_coords(local_left,2);
                              
               x_bar = sqrt(dx^2 + dy^2);
               
               y_bar_plus  = sqrt(DX_plus^2 + DY_plus^2);
               y_bar_minus = sqrt(DX_minus^2 + DY_minus^2);
               
               bond_vec_plus =  [DX_plus /y_bar_plus  DY_plus /y_bar_plus ]*s;
               bond_vec_minus = [DX_minus/y_bar_minus DY_minus/y_bar_minus]*s;
               
               % dilatational component
               value_plus = 2.0*kappa*dilat_plus*x_bar/m ...
                   + alpha*((y_bar_plus-x_bar)-dilat_plus*x_bar/2.0);
               value_plus_x = value_plus * bond_vec_plus(1);
               value_plus_y = value_plus * bond_vec_plus(2);
               value_minus = 2.0*kappa*dilat_minus*x_bar/m ...
                   + alpha*((y_bar_minus-x_bar)-dilat_minus*x_bar/2.0);
               value_minus_x = value_minus * bond_vec_minus(1);
               value_minus_y = value_minus * bond_vec_minus(2);
               
               %value_plus_x
               %value_minus_x
               
               value_x = (value_plus_x - value_minus_x) / (2.0*epsilon);
               value_y = (value_plus_y - value_minus_y) / (2.0*epsilon);
               
               left_x_index  = (left_node-1)*2 + 1;
               left_y_index  = left_x_index + 1;
               right_x_index = (right_node-1)*2 + 1;
               right_y_index = right_x_index + 1;
               
               tangent(left_x_index, tangent_col) = tangent(left_x_index, tangent_col) + value_x;
               tangent(left_y_index, tangent_col) = tangent(left_y_index, tangent_col) + value_y;
               tangent(right_x_index,tangent_col) = tangent(right_x_index,tangent_col) - value_x;
               tangent(right_y_index,tangent_col) = tangent(right_y_index,tangent_col) - value_y;
               
           end
       end
    end
end


end

