%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [ total_reaction ] = reaction( dim, num_nodes_x, num_nodes_y, internal_force)

% dim is the direction normal to the edge
% 1 is side, 2 is top

total_reaction = zeros(2,1);

if(dim~=1&&dim~=2)
    error('INVALID DIM');
end

if(dim==1)
    reaction = zeros(num_nodes_y,2);
    for i=1:num_nodes_y;
        stride = (i-1)*(num_nodes_x*2);
        reaction(i,1) = reaction(i,1) + internal_force(stride+1);
        reaction(i,2) = reaction(i,2) + internal_force(stride+2);
        if num_nodes_x > 3
            reaction(i,1) = reaction(i,1) + internal_force(stride+3);
            reaction(i,2) = reaction(i,2) + internal_force(stride+4);
        end
        if num_nodes_x > 5
            reaction(i,1) = reaction(i,1) + internal_force(stride+5);
            reaction(i,2) = reaction(i,2) + internal_force(stride+6);
        end
    end
end
if(dim==2)
    reaction = zeros(num_nodes_x,2);
    for i=1:num_nodes_x;
        stride = (i-1)*2;
        reaction(i,1) = reaction(i,1) + internal_force(stride+1);
        reaction(i,2) = reaction(i,2) + internal_force(stride+2);
        if num_nodes_y > 3
            stride = (i-1)*2 + num_nodes_x*2;
            reaction(i,1) = reaction(i,1) + internal_force(stride+1);
            reaction(i,2) = reaction(i,2) + internal_force(stride+2);
        end
        if num_nodes_y > 5
            stride = (i-1)*2 + num_nodes_x*2*2;
            reaction(i,1) = reaction(i,1) + internal_force(stride+1);
            reaction(i,2) = reaction(i,2) + internal_force(stride+2);
        end

    end
end

 
% 
% for i = 1:num_reactions
%     col_index_1 = i*(num_nodes_x*2) - 1;
%     reaction(i,1) = reaction(i,1) + (-1.0*internal_force(col_index_1));
%     col_index_1 = i*(num_nodes_x*2);
%     reaction(i,2) = reaction(i,2) + (-1.0*internal_force(col_index_1));
%     if num_nodes_x > 3
%         col_index_2 = i*(num_nodes_x*2) - 3;
%         reaction(i,1) = reaction(i,1) + (-1.0*internal_force(col_index_2));
%         col_index_2 = i*(num_nodes_x*2) - 2;
%         reaction(i,2) = reaction(i,2) + (-1.0*internal_force(col_index_2));
%     end
%     if num_nodes_x > 5
%         col_index_3 = i*(num_nodes_x*2) - 5;
%         reaction(i,1) = reaction(i,1) + (-1.0*internal_force(col_index_3));
%         col_index_3 = i*(num_nodes_x*2) - 4;
%         reaction(i,2) = reaction(i,2) + (-1.0*internal_force(col_index_3));
%     end
% end
total_reaction(1) = sum(reaction(:,1));
total_reaction(2) = sum(reaction(:,2));

end

