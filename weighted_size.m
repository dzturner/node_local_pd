%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [ weighted_size, ...
    dilat_weighted_size, ...
    dev_weighted_size ] = weighted_size(num_nodes_x, num_nodes_y, ...
    i_bonds, d_bonds, initial_coords, cell_size)

weighted_size        = zeros(num_nodes_x*num_nodes_y,1);
dilat_weighted_size  = zeros(num_nodes_x*num_nodes_y,1);
dev_weighted_size    = zeros(num_nodes_x*num_nodes_y,1);

num_i_bonds = length(i_bonds(:,1));
num_d_bonds = length(d_bonds(:,1));

for i=1:num_i_bonds
    left_node = i_bonds(i,1);
    right_node = i_bonds(i,2);
    dx = initial_coords(right_node,1) - initial_coords(left_node,1);
    dy = initial_coords(right_node,2) - initial_coords(left_node,2);
    x_bar = sqrt(dx^2 + dy^2);
    s_l = cell_size(left_node);
    s_r = cell_size(right_node);
    dilat_weighted_size(left_node)  = dilat_weighted_size(left_node) + ...
                                x_bar * x_bar * s_r;
    dilat_weighted_size(right_node) = dilat_weighted_size(right_node) + ...
                                x_bar * x_bar * s_l;
end

for i=1:num_d_bonds
    left_node = d_bonds(i,1);
    right_node = d_bonds(i,2);
    dx = initial_coords(right_node,1) - initial_coords(left_node,1);
    dy = initial_coords(right_node,2) - initial_coords(left_node,2);
    x_bar = sqrt(dx^2 + dy^2);
    s_l = cell_size(left_node);
    s_r = cell_size(right_node);
    dev_weighted_size(left_node)  = dev_weighted_size(left_node) + ...
                                x_bar * x_bar * s_r;
    dev_weighted_size(right_node) = dev_weighted_size(right_node) + ...
                                x_bar * x_bar * s_l;
end

weighted_size = dilat_weighted_size + dev_weighted_size;

% for i=1:num_bonds
%     left_node = bonds(i,1);
%     right_node = bonds(i,2);
%     dx = initial_coords(right_node,1) - initial_coords(left_node,1);
%     dy = initial_coords(right_node,2) - initial_coords(left_node,2);
%     x_bar = sqrt(dx^2 + dy^2);
%     s_l = cell_size(left_node);
%     s_r = cell_size(right_node);
%     weighted_size(left_node)  = weighted_size(left_node) + ...
%                                 x_bar * x_bar * s_r;
%     weighted_size(right_node) = weighted_size(right_node) + ...
%                                 x_bar * x_bar * s_l;
% end

end

