%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [ strain_energy_density ] = strain_energy_density_dual(K, mu, h, ...
    num_elem, local_bonds, connectivity, dilat_dual, initial_coords, current_coords)

strain_energy_density  = zeros(num_elem,1);
num_local_bonds        = length(local_bonds(:,1));

m     = 8*h^4;
s     = h^4;
kappa = K + mu/3.0;
alpha = 8.0*mu/m;


% shear component
for i = 1:num_elem

    for bond = 1:num_local_bonds
    
        left_node = connectivity(i,local_bonds(bond,1));
        right_node = connectivity(i,local_bonds(bond,2));
        dx = initial_coords(right_node,1) - initial_coords(left_node,1);
        dy = initial_coords(right_node,2) - initial_coords(left_node,2);
        DX = current_coords(right_node,1) - current_coords(left_node,1);
        DY = current_coords(right_node,2) - current_coords(left_node,2);
        x_bar = sqrt(dx^2 + dy^2);
        y_bar = sqrt(DX^2 + DY^2);

        theta_dual = dilat_dual(i);

        e = ((y_bar-x_bar)-theta_dual*x_bar/2.0);
        value = 0.5*alpha*e*e*s;
        
        strain_energy_density(i) = strain_energy_density(i) + value;

    end
    
end

% dilat component
for i = 1:num_elem
    
    theta_dual = dilat_dual(i);
    value = 0.5*kappa*theta_dual*theta_dual;
    strain_energy_density(i) = strain_energy_density(i) + value;
    
end


end

