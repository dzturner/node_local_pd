%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [  ] = post_process(dim,num_nodes, residual, initial_coords, ...
    current_coords, ...
    weighted_size, dev_weighted_size,...
    displacement, internal_force)

 resid_x = zeros(num_nodes,1);
 for i = 1:num_nodes
     resid_x(i) = residual((i-1)*2 + 1);
 end
 internal_force_x = zeros(num_nodes,1);
 for i = 1:num_nodes
     internal_force_x(i) = internal_force((i-1)*2 + 1);
 end
 internal_force_y = zeros(num_nodes,1);
 for i = 1:num_nodes
     internal_force_y(i) = internal_force((i-1)*2 + 2);
 end
 
 %resid_x
 %labels = cellstr( num2str(resid_x) );
 labels = cellstr( num2str(displacement(:,dim)) );
 %labels = cellstr( num2str(internal_force_x) );
 %labels = cellstr( num2str(internal_force_y) );
 %labels = cellstr( num2str(strain_energy_density) );
 %labels = cellstr( num2str(weighted_size) );
 %labels = cellstr( num2str(dev_weighted_size) );

 hold on
 plot(initial_coords(:,1),initial_coords(:,2),'bo')
 plot(current_coords(:,1),current_coords(:,2),'kx')
 text(current_coords(:,1),current_coords(:,2),labels,...
    'VerticalAlignment'  ,'bottom', ...
    'HorizontalAlignment','left')
end

