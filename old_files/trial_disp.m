clear; clc;



gamma_deg = [0:0.01:45]';
gamma = gamma_deg.*pi./180;

error = zeros(length(gamma(:,1)),1);

for i=1:length(gamma(:,1))

Coords = [0 0
          1 0
          1 1
          0 1];
      
for j = 1:4
   x = Coords(j,1);
   y = Coords(j,2);

   u = [ x*(cos(gamma(i)) - 1) + y*sin(gamma(i))
      x*sin(gamma(i)) + y*(cos(gamma(i))-1) ]';
   Coords(j,:) = Coords(j,:) + u; 
end
  Coords;
  
dxAB = Coords(2,1) - Coords(1,1);
dxBC = Coords(3,1) - Coords(2,1);
dxCD = Coords(4,1) - Coords(3,1);
dxDA = Coords(1,1) - Coords(4,1);
dyAB = Coords(2,2) - Coords(1,2);
dyBC = Coords(3,2) - Coords(2,2);
dyCD = Coords(4,2) - Coords(3,2);
dyDA = Coords(1,2) - Coords(4,2);

dxAC = Coords(3,1) - Coords(1,1);
dxDB = Coords(4,1) - Coords(2,1);
dyAC = Coords(3,2) - Coords(1,2);
dyDB = Coords(4,2) - Coords(2,2);

LengthAB = sqrt(dxAB^2 + dyAB^2);
LengthBC = sqrt(dxBC^2 + dyBC^2);
LengthCD = sqrt(dxCD^2 + dyCD^2);
LengthDA = sqrt(dxDA^2 + dyDA^2);

AC = abs(sqrt(dxAC^2 + dyAC^2) - sqrt(2));
DB = abs(sqrt(dxDB^2 + dyDB^2) - sqrt(2));

error(i) = abs(AC - DB);

end
error;
plot(gamma_deg,error)