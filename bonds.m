%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [ bonds, num_bonds, ...
           i_bonds, num_i_bonds, ...
           d_bonds, num_d_bonds ] = bonds( num_nodes_x, num_nodes_y )

% generate the list of straight bonds and diagonal bonds
num_i_bonds = (num_nodes_x-1)*num_nodes_y + (num_nodes_y-1)*num_nodes_x;
i_bonds = zeros(num_i_bonds,2);
num_d_bonds = (num_nodes_x-1)*(num_nodes_y-1) * 2;
d_bonds = zeros(num_d_bonds,2);
num_bonds = num_i_bonds + num_d_bonds;

bond_index = 1;

% horizontal bonds
for i=1:num_nodes_y
  for j=1:num_nodes_x-1
      node_left = (i-1)*num_nodes_x + j;
      node_right = node_left + 1;
      i_bonds(bond_index,1) = node_left;
      i_bonds(bond_index,2) = node_right;
      bond_index = bond_index + 1;
  end
end
% vertical bonds
for i=1:num_nodes_y-1
  for j=1:num_nodes_x
      node_bottom = (i-1)*num_nodes_x + j;
      node_top = i*num_nodes_x + j;
      i_bonds(bond_index,1) = node_bottom;
      i_bonds(bond_index,2) = node_top;
      bond_index = bond_index + 1;
  end
end
% i_bonds

% diagonal bonds
bond_index = 1;
for i=1:num_nodes_y-1
  for j=1:num_nodes_x-1
      node_bottom_left = (i-1)*num_nodes_x + j;
      node_top_left = i*num_nodes_x + j;
      node_bottom_right = node_bottom_left + 1;
      node_top_right = node_top_left + 1;
      d_bonds(bond_index,1) = node_bottom_left;
      d_bonds(bond_index,2) = node_top_right;
      bond_index = bond_index + 1;
      d_bonds(bond_index,1) = node_bottom_right;
      d_bonds(bond_index,2) = node_top_left;
      bond_index = bond_index + 1;
  end
end
% d_bonds
bonds = [ i_bonds ; d_bonds ];

end

