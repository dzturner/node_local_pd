%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

%   local node ordering:
%
%   4----3
%   |    |
%   1----2
%

function [ connectivity_dilat ] = connectivity_dilat(num_nodes_x, num_nodes_y)

connectivity_dilat = zeros(num_nodes_x*num_nodes_y,4);


% corner nodes:
connectivity_dilat(1,1:4) = ...
    [1 2 (num_nodes_x + 2) (num_nodes_x + 1)];
connectivity_dilat(num_nodes_x,1:4) = ...
    [(num_nodes_x - 1) num_nodes_x (2*num_nodes_x) (2*num_nodes_x - 1)];
connectivity_dilat((num_nodes_y-1)*num_nodes_x+1,1:4) = ...
    [((num_nodes_y-2)*num_nodes_x+1) ((num_nodes_y-2)*num_nodes_x+2) ((num_nodes_y-1)*num_nodes_x +2) ((num_nodes_y-1)*num_nodes_x + 1)];
connectivity_dilat(num_nodes_y*num_nodes_x,1:4) = ...
    [((num_nodes_y-1)*num_nodes_x - 1) (num_nodes_y-1)*num_nodes_x num_nodes_y*num_nodes_x (num_nodes_y*num_nodes_x-1)];

% edge_nodes
% left and right
for i=2:num_nodes_y - 1
    node_left  = (i-1)*num_nodes_x + 1;
    connectivity_dilat(node_left,1:4) = ...
        [(node_left-num_nodes_x) (node_left-num_nodes_x+1) (node_left+num_nodes_x+1) (node_left+num_nodes_x)];
    node_right = node_left + num_nodes_x - 1;
    connectivity_dilat(node_right,1:4) = ...
        [(node_right-num_nodes_x-1) (node_right-num_nodes_x) (node_right+num_nodes_x) (node_right+num_nodes_x-1) ];
end

% edge_nodes
% top and bottom
for i=2:num_nodes_x - 1
    node_bottom  = i;
    connectivity_dilat(node_bottom,1:4) = ...
        [(node_bottom-1) (node_bottom+1) (node_bottom+num_nodes_x+1) (node_bottom+num_nodes_x-1)];
    node_top = node_bottom + (num_nodes_y-1)*num_nodes_x;
    connectivity_dilat(node_top,1:4) = ...
        [(node_top-num_nodes_x-1) (node_top-num_nodes_x+1) (node_top+1) (node_top-1) ];
end

% middle nodes
for i=2:num_nodes_y-1
    for j=2:num_nodes_x-1
        node = (i-1)*num_nodes_x + j;
        connectivity_dilat(node,1:4) = ...
            [(node-num_nodes_x-1) (node-num_nodes_x+1) (node+num_nodes_x+1) (node+num_nodes_x-1)];
    end
end

end

