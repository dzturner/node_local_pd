%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [ strain_energy_density ] = strain_energy_density(num_nodes, ...
    num_bonds, bonds, initial_coords, current_coords, dilat, weighted_size,...
    cell_size, K, mu)

strain_energy_density = zeros(num_nodes,1);

% add the deviatoric portion
for i=1:num_bonds
    left_node = bonds(i,1);
    right_node = bonds(i,2);
    dx = initial_coords(right_node,1) - initial_coords(left_node,1);
    dy = initial_coords(right_node,2) - initial_coords(left_node,2);
    DX = current_coords(right_node,1) - current_coords(left_node,1);
    DY = current_coords(right_node,2) - current_coords(left_node,2);
    x_bar = sqrt(dx^2 + dy^2);
    y_bar = sqrt(DX^2 + DY^2);
    
    theta_l = dilat(left_node);
    theta_r = dilat(right_node);
    m_l     = weighted_size(left_node);
    m_r     = weighted_size(right_node);
    s_l     = cell_size(left_node);
    s_r     = cell_size(right_node);

    ed_l = (y_bar - x_bar) - theta_l*x_bar/2.0;
    ed_r = (y_bar - x_bar) - theta_r*x_bar/2.0;
    
    %K_tilda = K + mu/3.0;

    alpha_l = 6*mu/m_l;
    alpha_r = 6*mu/m_r;  
    
   strain_energy_density(left_node) = strain_energy_density(left_node) ...
       + alpha_l/2.0 * ed_l * ed_l * s_r; 
   strain_energy_density(right_node) = strain_energy_density(right_node) ...
       + alpha_r/2.0 * ed_r * ed_r * s_l; 
end

% add the dilatation portion
for i=1:num_nodes
    theta = dilat(i); 
    K_tilda = K + mu/3.0;
    
   strain_energy_density(i) = strain_energy_density(i) ...
       + K_tilda/2.0 * theta*theta; 
end

end

