%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.
%
%  DESCRIPTION:

function [ c, ceq ] = adjoint_constraint( x )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

force_mag = -1000.0;

E_0  = 100.00E3; % How's it putting in E and nu for a change, Callie?:)
nu_0 = 0.25;

epsilon = 1E-3;

% square domain
length = 1.0;
% length must be divisable by h
h = length / 10.0;
%length + h;
num_elem_x = length / h;
num_elem_y = length / h;
num_nodes_x = num_elem_x + 1;
num_nodes_y = num_elem_y + 1;
num_nodes = num_nodes_x * num_nodes_y;

c = zeros(num_nodes);
ceq = zeros(num_nodes);

% create the sensetivity stencils:
[ node_stencil, node_sens ] = node_stencil(num_nodes_x, num_nodes_y);

% create the node grid coords
initial_coords = coords(num_nodes_x, num_nodes_y, h);
current_coords = initial_coords;

% converted material properties:
r = zeros(num_nodes,1);
for i = 1:num_nodes
    r(i) = initial_coords(i,1)*initial_coords(i,1) + ...
        initial_coords(i,2)*initial_coords(i,2);
end
r = sqrt(r);
K_0  = E_0 / (3*(1-2*nu_0));
mu_0 = E_0 / (2*(1+nu_0));

factor = 0.25;
%K = factor.*r.*K_0 + K_0;
mu = -factor.*r.*mu_0 + mu_0;
K = x;

% generate the list of straight bonds and diagonal bonds
[ bonds, num_bonds, ...
  i_bonds, num_i_bonds, ...
  d_bonds, num_d_bonds] = bonds(num_nodes_x, num_nodes_y);

% compute the cell sizes
cell_size = cell_size(num_nodes_x, num_nodes_y, h);

% compute the weighted size
[ weighted_size, ...
  dilat_weighted_size, ...
  dev_weighted_size ] = weighted_size(num_nodes_x, num_nodes_y, ...
                            i_bonds, d_bonds, initial_coords, cell_size);
 
% initialize displacement field
displacement = zeros(num_nodes,2);

% allocate the tangent matrix
tangent = zeros(num_nodes*2,num_nodes*2);

    % boundary conditions
    [ displacement, bc_flags ] = boundary_conditions('force_compression_y', ...
         initial_coords, displacement, num_nodes_x, num_nodes_y);
    %displacement
     
    current_coords = initial_coords + displacement;

    % compute the dilataion
    it_dilat =  dilat(num_nodes, num_bonds, bonds, initial_coords, ...
    current_coords, weighted_size, cell_size);
    
    %compute the residual
    it_residual = zeros(num_nodes*2,1);
    RHS = zeros(num_nodes*2,1);
    %it_stab = zeros(num_nodes,1);
    for node=1:num_nodes
        for dim=1:2
            resid_index = (node-1)*2 + dim;
            it_residual(resid_index) = residual_adjoint(node,dim,node_stencil,...
                initial_coords, current_coords, cell_size, weighted_size, it_dilat,...
                K, mu);
        end
    end
    %it_residual
    internal_force = it_residual;
    
    % apply bcs to residual    
    for i=1:num_nodes*2
        if(bc_flags(i)==1)
            it_residual(i)  = 0.0;
        end
    end
    %it_residual
    
    % add the external force
    force_it = 0;
    stride_start = (num_nodes_y-1)*num_nodes_x*2;
    for n=1:num_nodes_x
        force_it = force_it + 2; % only hit the y-comps
        force_val = force_mag/(num_nodes_x-1);
        if n==1 || n==num_nodes_x
            force_val = force_val/2;
        end
        RHS(stride_start + force_it) = RHS(stride_start + force_it)...
            + force_val;
    end
    %it_residual
    
    % test the residual norm
    fprintf('\nIt: %i Residual: %f\n\n', iter,norm(it_residual))
    % test the residual norm
    %if(norm(it_residual) < tol)
    %    fprintf('\n-----------------\n Step Converged!\n-----------------\n'); 
    %    break;
    %end
    %if(iter==max_its+1)
    %    fprintf('\n-----------------\n Step Failed!\n-----------------\n');
    %    %
    %    %error();
    %end
    
    % only compute the tangent on the first iteration
    if (iter==1)
        fprintf('STATUS: Computing the tangent...\n')
        % initialize temp arrays
        coords_plus  = current_coords;
        coords_minus = current_coords;
        dilat_plus   = zeros(num_nodes,1);
        dilat_minus  = zeros(num_nodes,1);
        
        num_sens = 25;
        for node=1:num_nodes
            for dim=1:2
                row = (node-1)*2 + dim;
                for sens = 1:num_sens
                    pert_node = node_sens(node,sens);
                    if(pert_node==0);continue; end
                    for pert_dim = 1:2
                        col = (pert_node-1)*2 + pert_dim;
                        % perturb the coordinates
                        coords_plus(pert_node,pert_dim) = ...
                            coords_plus(pert_node,pert_dim) + epsilon;
                        coords_minus(pert_node,pert_dim) = ...
                            coords_minus(pert_node,pert_dim) - epsilon;
                        
                        % compute the perturbed dilats
                        dilat_plus = dilat(num_nodes, num_bonds, bonds, initial_coords, ...
                            coords_plus, weighted_size, cell_size);
                        dilat_minus = dilat(num_nodes, num_bonds, bonds, initial_coords, ...
                            coords_minus, weighted_size, cell_size);
                        
                        % calculate the tangent contribs
                         resid_plus = residual_adjoint(node,dim,node_stencil,initial_coords,...
                             coords_plus, cell_size, weighted_size, dilat_plus, K, mu);
                         resid_minus = residual_adjoint(node,dim,node_stencil,initial_coords,...
                             coords_minus, cell_size, weighted_size, dilat_minus, K, mu);
                        
                        value = (resid_plus - resid_minus)/(2*epsilon);
                        
                        tangent(row,col) = tangent(row,col) + value;
                        
                        % reset the coordinates
                        coords_plus(pert_node,pert_dim) = ...
                            coords_plus(pert_node,pert_dim) - epsilon;
                        coords_minus(pert_node,pert_dim) = ...
                            coords_minus(pert_node,pert_dim) + epsilon;
                    end
                end
            end
        end
        %tangent
        
         % apply bcs to the tangent
         for i=1:num_nodes*2
             if(bc_flags(i)==1)
                 tangent(i,:) = 0;
                 tangent(:,i) = 0;
                 tangent(i,i) = 1.0/epsilon;
                 % multiply by the stiffness to scale better
             end
         end
        %tangent
        fprintf('STATUS: Done computing the tangent\n')
    end
    
    fprintf('STATUS: Solving...\n')
    %lhs = inv(tangent) * (-it_residual);
    lhs = tangent \ -RHS;
    fprintf('STATUS: Done solving\n')
    
     
end

