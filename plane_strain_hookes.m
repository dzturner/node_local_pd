
%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.
%
%  DESCRIPTION:
%
% Node local peridynamics is a simple 2D peridynamics code
% for regular affine grids that uses the standard FEM stencil
% as the horizon. This local restriction of the horizon leads
% to a formulation equivalent to classical solid mechanics, but
% is integral based and naturally incorporates discontinuities.

function [ sigma_xx, sigma_yy, tau_xy ] = plane_strain_hookes( ...
    K, mu, e_xx, e_yy, gamma_xy)

E = 9*K*mu/(3*K + mu);
nu = (3*K - 2*mu)/(2*(3*K + mu));

coef = E / ((1+nu)*(1-2*nu));

sigma_xx = coef*((1-nu)*e_xx + nu*e_yy);
sigma_yy = coef*(nu*e_xx + (1-nu)*e_yy);

% note that if gamma from the bcs's is prescribed then here 
% it must be multiplied by 2
tau_xy   =  mu*gamma_xy*2;

end

