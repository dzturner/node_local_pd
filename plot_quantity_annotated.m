%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [  ] = plot_quantity_annotated( vector, num_elem, initial_coords, current_coords, ...
    connectivity)


is_elem_scalar = false;
vec_size = size(vector);
if(vec_size(1,1)==num_elem)
    is_elem_scalar = true;
end

if is_elem_scalar

    cell_coords = zeros(num_elem,2);
    
    labels_x = cellstr( num2str(vector(:,1)) );

    for i = 1:num_elem
        cx = 0.0;
        cy = 0.0;
        for j=1:4
            node = connectivity(i,j);
            cx = cx + initial_coords(node,1);
            cy = cy + initial_coords(node,2);
        end
        cx = cx / 4.0;
        cy = cy / 4.0;
        cell_coords(i,1) = cx;
        cell_coords(i,2) = cy;
    end
    
    hold on
    plot(initial_coords(:,1),initial_coords(:,2),'bo')
    plot(current_coords(:,1),current_coords(:,2),'kx')
    text(cell_coords(:,1),cell_coords(:,2),labels_x,...
        'VerticalAlignment'  ,'bottom', ...
        'HorizontalAlignment','left')

else
    
    width = vec_size(1,2);
    
    switch width
        case 2 % multi vec
            labels_x = cellstr( num2str(vector(:,1)) );
            labels_y = cellstr( num2str(vector(:,2)) );
        case 1 % column vec
            vec_length = length(vector(:,1));
            multi_vec = zeros(vec_length / 2,2);
            for i = 1:vec_length/2
                multi_vec(i,1) = vector((i-1)*2 + 1);
                multi_vec(i,2) = vector((i-1)*2 + 2);
            end
            labels_x = cellstr( num2str(multi_vec(:,1)) );
            labels_y = cellstr( num2str(multi_vec(:,2)) );
        otherwise
            error('');
    end
    
    hold on
    plot(initial_coords(:,1),initial_coords(:,2),'bo')
    plot(current_coords(:,1),current_coords(:,2),'kx')
    text(current_coords(:,1),current_coords(:,2),labels_x,...
        'VerticalAlignment'  ,'bottom', ...
        'HorizontalAlignment','left')
    text(current_coords(:,1),current_coords(:,2),labels_y,...
        'VerticalAlignment'  ,'top', ...
        'HorizontalAlignment','left')
end

end

