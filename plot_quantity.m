%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [  ] = plot_quantity( vector, resolution, length_x, length_y, num_nodes_x, num_nodes_y, h)

% NOTE only accepts nodal vectors/scalars or element scalars

% check if it is a node or elem field
vec_size = size(vector);
total_entries = vec_size(1,1) * vec_size(1,2);
is_nodal  = false;
is_scalar = false;
switch total_entries
    % nodal scalar
    case num_nodes_x*num_nodes_y
        [X,Y] = meshgrid(0:h:length_x,0:h:length_y);
        values_x = zeros(num_nodes_x,num_nodes_y);
        is_nodal = true;
        is_scalar = true;
    % nodal vector
    case num_nodes_x*num_nodes_y*2
        % nodal vector
        [X,Y] = meshgrid(0:h:length_x,0:h:length_y);
        width = vec_size(1,2);
        values_x = zeros(num_nodes_x,num_nodes_y);
        values_y = zeros(num_nodes_x,num_nodes_y);
        is_nodal = true;
    % elem scalar
    case (num_nodes_x-1)*(num_nodes_y-1)
        % elem vector
        [X,Y] = meshgrid(h/2:h:length_x-h/2,...
                         h/2:h:length_y-h/2);
        width = vec_size(1,2);
        values_x = zeros((num_nodes_x-1),(num_nodes_y-1));
    otherwise
        error('NOT A VALID VECTOR SIZE FOR PLOTTING')
end


% convert the vector into a meshable array
if(is_nodal)
    if(is_scalar)
        vec_it = 1;
        for j=1:num_nodes_y
            for i=1:num_nodes_x
                values_x(j,i) = vector(vec_it,1);
                vec_it = vec_it + 1;
            end
        end
    else
        switch width
            case 2 % multi vec
                vec_it = 1;
                for j=1:num_nodes_y
                    for i=1:num_nodes_x
                        values_x(j,i) = vector(vec_it,1);
                        values_y(j,i) = vector(vec_it,2);
                        vec_it = vec_it + 1;
                    end
                end
            case 1 % column vec
                vec_it = 1;
                for j=1:num_nodes_y
                    for i=1:num_nodes_x
                        values_x(j,i) = vector((vec_it-1)*2 + 1);
                        values_y(j,i) = vector((vec_it-1)*2 + 2);
                        vec_it = vec_it + 1;
                    end
                end
            otherwise
                error('');
        end
    end
else
    vec_it = 1;
    for j=1:num_nodes_y-1
        for i=1:num_nodes_x-1
            values_x(j,i) = vector(vec_it);
            vec_it = vec_it + 1;
        end
    end
end

% more interpolated plots:
if(is_nodal)
    if(is_scalar)
        figure;        
        hold on
        for j=1:num_nodes_y
            for i = 1:num_nodes_x
                cx = X(i,j);
                cy = Y(i,j);
                color = values_x(i,j);
                
                x = [(cx-h/2) (cx+h/2) (cx+h/2) (cx-h/2)];
                y = [(cy-h/2) (cy-h/2) (cy+h/2) (cy+h/2)];
                c = [color color color color];
                fill(x,y,c)
            end
        end
        colorbar('location','eastoutside')
        figure;
        %surf(X,Y,values_x);
        surf(X,Y,values_x);
        colormap([1  1  1]);
        grid off;
        box off;
        view([-40.5,12]);
        set(gca,'FontSize',14)
        hold off
    else
        X_interp = interp2(X,resolution);
        Y_interp = interp2(Y,resolution);
        figure;
        values_x_interp = interp2(values_x,resolution);
        h = pcolor(X_interp,Y_interp,values_x_interp);
        set(h,'EdgeColor','none');
        colorbar('location','eastoutside')
        figure;
        values_y_interp = interp2(values_y,resolution);
        h = pcolor(X_interp,Y_interp,values_y_interp);
        set(h,'EdgeColor','none');
        colorbar('location','eastoutside')
    end
else
    figure;
    hold on
    for j=1:num_nodes_y-1
        for i = 1:num_nodes_x-1
            cx = X(i,j);
            cy = Y(i,j);
            color = values_x(i,j);
            
            x = [(cx-h/2) (cx+h/2) (cx+h/2) (cx-h/2)];
            y = [(cy-h/2) (cy-h/2) (cy+h/2) (cy+h/2)];
            c = [color color color color];
            fill(x,y,c)
        end
    end
    colorbar('location','eastoutside')
    figure;
    surf(X,Y,values_x);
    hold off
    %figure;
    %pcolor(X,Y,values_x)
    %colorbar('location','eastoutside')
end
end

