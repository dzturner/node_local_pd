%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [ dilat ] = dilat(num_nodes, num_bonds, bonds, initial_coords, ...
    current_coords, weighted_size, cell_size)

dilat          = zeros(num_nodes,1);

for i=1:num_bonds
    left_node = bonds(i,1);
    right_node = bonds(i,2);
    dx = initial_coords(right_node,1) - initial_coords(left_node,1);
    dy = initial_coords(right_node,2) - initial_coords(left_node,2);
    DX = current_coords(right_node,1) - current_coords(left_node,1);
    DY = current_coords(right_node,2) - current_coords(left_node,2);
    x_bar = sqrt(dx^2 + dy^2);
    y_bar = sqrt(DX^2 + DY^2);
    s_l = cell_size(left_node);
    s_r = cell_size(right_node);
    m_l = weighted_size(left_node);
    m_r = weighted_size(right_node);
    
    dilat(left_node) = dilat(left_node) ...
                     + 2.0 * (y_bar - x_bar) * x_bar * s_r/m_l;
    dilat(right_node) = dilat(right_node) ...
                     + 2.0 * (y_bar - x_bar) * x_bar * s_l/m_r;
end



end

