%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [ tangent ] = stab_tangent_dual(K, mu, h, epsilon,...
    num_nodes, num_elem, ...
    local_bonds, connectivity, dilat_dual, initial_coords, current_coords,...
    elem_connectivity)

tangent = zeros(num_nodes*2,num_nodes*2);

num_local_bonds       = length(local_bonds(:,1));

m     = 8*h^4;
s     = h^4;
kappa = K + mu/3.0;
alpha = 8.0*mu/m;


coords_plus  = current_coords;
coords_minus = current_coords;

avg_stencil = [ 1 2 3 5; 1 3 4 6; 1 6 8 9; 1 5 7 8];

for i=1:num_elem
    %construct the sensetivity nodes
    sens_nodes = zeros(1,16);
    % add your own nodes first
    sens_nodes(1:4) = connectivity(i,:);
    sens_index = 4;
    for neigh_it=1:8
       neigh_elem = elem_connectivity(i,neigh_it);
       if neigh_elem==0; continue; end
       for nn = 1:4
           neigh_node = connectivity(neigh_elem,nn);
           % search the list to see if this node is already in there
           node_found = false;
           for s=1:sens_index
               if neigh_node == sens_nodes(s)
                   node_found = true;
                   break
               end
           end
           if node_found == false
               sens_index = sens_index + 1;
               sens_nodes(sens_index) = neigh_node;
           end
       end
    end
    %sens_nodes
    
    % calculate the perturbations for each sens node
    num_sens_nodes = sens_index;
    elem_stencil = zeros(9,1);
    elem_stencil(1) = i;
    elem_stencil(2:9) = elem_connectivity(i,:);
    %elem_stencil
    for s_node = 1:num_sens_nodes
        for dim =1:2
            %pert the coords
            node = sens_nodes(s_node);
            tangent_col = (node-1)*2 + dim;
            coords_plus(node,dim)  = coords_plus(node,dim) + epsilon;
            coords_minus(node,dim) = coords_minus(node,dim) - epsilon;

            pert_dilat_plus  = zeros(9,1);
            pert_dilat_minus = zeros(9,1);
            for e = 1:9
                elem = elem_stencil(e);
                if elem==0; continue; end
                
                %compute pert dilat for this elem
                dilat_plus = 0.0;
                dilat_minus = 0.0;
                for bond = 1:num_local_bonds
                    
                    local_left = local_bonds(bond,1);
                    local_right = local_bonds(bond,2);
                    left_node = connectivity(elem,local_left);
                    right_node = connectivity(elem,local_right);
                    
                    dx = initial_coords(right_node,1) - initial_coords(left_node,1);
                    dy = initial_coords(right_node,2) - initial_coords(left_node,2);
                    
                    DX_plus = coords_plus(right_node,1) - coords_plus(left_node,1);
                    DY_plus = coords_plus(right_node,2) - coords_plus(left_node,2);
                    DX_minus = coords_minus(right_node,1) - coords_minus(left_node,1);
                    DY_minus = coords_minus(right_node,2) - coords_minus(left_node,2);
                    
                    x_bar = sqrt(dx^2 + dy^2);
                    
                    y_bar_plus  = sqrt(DX_plus^2 + DY_plus^2);
                    y_bar_minus = sqrt(DX_minus^2 + DY_minus^2);
                    
                    dilat_plus  = dilat_plus + 2.0 * (y_bar_plus - x_bar) * x_bar * s / m;
                    dilat_minus = dilat_minus + 2.0 * (y_bar_minus - x_bar) * x_bar * s / m;
                    
                    pert_dilat_plus(e)  = dilat_plus;
                    pert_dilat_minus(e) = dilat_minus;
                    
                end
            end
            
            %pert_dilat_plus
            %pert_dilat_minus
            
            % now compute the averages on the four nodes of this elem:
            pert_dilat_tilde_plus  = zeros(4,1);
            pert_dilat_tilde_minus = zeros(4,1);
            num_pert_entries = zeros(4,1);
            for ni = 1:4
                for nj = 1:4
                    pert_id = avg_stencil(ni,nj);
                    if (pert_id==0); continue; end
                    pert_dilat_tilde_plus(ni) = pert_dilat_tilde_plus(ni) + ...
                        pert_dilat_plus(pert_id);
                    pert_dilat_tilde_minus(ni) = pert_dilat_tilde_minus(ni) + ...
                        pert_dilat_minus(pert_id);
                    num_pert_entries(ni) = num_pert_entries(ni) + 1;
                end
            end
            pert_dilat_tilde_plus  = pert_dilat_tilde_plus./num_pert_entries;
            pert_dilat_tilde_minus = pert_dilat_tilde_minus./num_pert_entries;
            
            my_pert_dilat_plus = pert_dilat_plus(1);
            my_pert_dilat_minus = pert_dilat_minus(1);
            
            % calculate the contributions
            for bond = 1:num_local_bonds
               
               local_left = local_bonds(bond,1);
               local_right = local_bonds(bond,2);
               left_node = connectivity(i,local_left);
               right_node = connectivity(i,local_right);
               
               dx = initial_coords(right_node,1) - initial_coords(left_node,1);
               dy = initial_coords(right_node,2) - initial_coords(left_node,2);
               
               DX_plus = coords_plus(right_node,1) - coords_plus(left_node,1);
               DY_plus = coords_plus(right_node,2) - coords_plus(left_node,2);
               DX_minus = coords_minus(right_node,1) - coords_minus(left_node,1);
               DY_minus = coords_minus(right_node,2) - coords_minus(left_node,2);
                              
               x_bar = sqrt(dx^2 + dy^2);
               
               y_bar_plus  = sqrt(DX_plus^2 + DY_plus^2);
               y_bar_minus = sqrt(DX_minus^2 + DY_minus^2);
               
               bond_vec_plus =  [DX_plus /y_bar_plus  DY_plus /y_bar_plus ]*s;
               bond_vec_minus = [DX_minus/y_bar_minus DY_minus/y_bar_minus]*s;
               
               % right contribs
               value_plus = 0.5*kappa*abs(my_pert_dilat_plus - pert_dilat_tilde_plus(local_right))*x_bar/m;
               value_plus_x = value_plus * bond_vec_plus(1);
               value_plus_y = value_plus * bond_vec_plus(2);
               value_minus = 0.5*kappa*abs(my_pert_dilat_minus - pert_dilat_tilde_minus(local_right))*x_bar/m;
               value_minus_x = value_minus * bond_vec_minus(1);
               value_minus_y = value_minus * bond_vec_minus(2);
               
               value_x = (value_plus_x - value_minus_x) / (2.0*epsilon);
               value_y = (value_plus_y - value_minus_y) / (2.0*epsilon);
               
               right_x_index = (right_node-1)*2 + 1;
               right_y_index = right_x_index + 1;
               
               tangent(right_x_index,tangent_col) = tangent(right_x_index,tangent_col) - value_x;
               tangent(right_y_index,tangent_col) = tangent(right_y_index,tangent_col) - value_y;

               % left contribs
               value_plus = 0.5*kappa*abs(my_pert_dilat_plus - pert_dilat_tilde_plus(local_left))*x_bar/m;
               value_plus_x = value_plus * bond_vec_plus(1);
               value_plus_y = value_plus * bond_vec_plus(2);
               value_minus = 0.5*kappa*abs(my_pert_dilat_minus - pert_dilat_tilde_minus(local_left))*x_bar/m;
               value_minus_x = value_minus * bond_vec_minus(1);
               value_minus_y = value_minus * bond_vec_minus(2);
               
               value_x = (value_plus_x - value_minus_x) / (2.0*epsilon);
               value_y = (value_plus_y - value_minus_y) / (2.0*epsilon);
               
               left_x_index  = (left_node-1)*2 + 1;
               left_y_index  = left_x_index + 1;
               
               tangent(left_x_index, tangent_col) = tangent(left_x_index, tangent_col) + value_x;
               tangent(left_y_index, tangent_col) = tangent(left_y_index, tangent_col) + value_y;
               
           end
            
           %unpert the coords
           coords_plus(node,dim)  = coords_plus(node,dim) - epsilon;
           coords_minus(node,dim) = coords_minus(node,dim) + epsilon;
        end
    end
    
    
    
end

end

