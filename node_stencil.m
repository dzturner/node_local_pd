%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.
%
%  DESCRIPTION:
%
% Node local peridynamics is a simple 2D peridynamics code
% for regular affine grids that uses the standard FEM stencil
% as the horizon. This local restriction of the horizon leads
% to a formulation equivalent to classical solid mechanics, but
% is integral based and naturally incorporates discontinuities.

function [ node_stencil node_sens] = node_stencil(num_nodes_x, num_nodes_y)

num_nodes = num_nodes_x*num_nodes_y;

node_stencil = zeros(num_nodes,8);
node_sens    = zeros(num_nodes,25);

for node = 1:num_nodes
    
    x = node - (ceil(node/num_nodes_x) - 1) * num_nodes_x;
    y = ceil(node/num_nodes_x);
    
    % Residual Stencil
    
    if( y-1>0 )
        if(x-1>0)
            node_stencil(node,1) = node - num_nodes_x - 1;
        end
        node_stencil(node,2) = node - num_nodes_x;
        if(x+1<=num_nodes_x)
            node_stencil(node,3) = node - num_nodes_x + 1;
        end
    end
    if( x-1>0 )
        node_stencil(node,4) = node - 1;
    end
    if(x+1<=num_nodes_x )
        node_stencil(node,5) = node + 1;
    end
    if( y+1<=num_nodes_y)
        if(x-1>0)
            node_stencil(node,6) = node + num_nodes_x - 1;
        end
        node_stencil(node,7) = node + num_nodes_x;
        if(x+1<=num_nodes_x)
            node_stencil(node,8) = node + num_nodes_x + 1;
        end
    end
    
    % Sensitivity Stencil
    
    node_sens(node,1) = node;
    node_sens(node,2:9) = node_stencil(node,:);
    
    if( y-2>0 )
        if(x-2>0)
            node_sens(node,10) = node - num_nodes_x*2 - 2;
        end
        if(x-1>0)
            node_sens(node,11) = node - num_nodes_x*2 - 1;
        end
        node_sens(node,12) = node - num_nodes_x*2;
        if(x+1<=num_nodes_x)
            node_sens(node,13) = node - num_nodes_x*2 + 1;
        end
        if(x+2<=num_nodes_x)
            node_sens(node,14) = node - num_nodes_x*2 + 2;
        end
    end
    if( y-1>0 )
        if(x-2>0)
            node_sens(node,15) = node - num_nodes_x - 2;
        end
        if(x+2<=num_nodes_x)
            node_sens(node,16) = node - num_nodes_x + 2;
        end
    end
    if(x-2>0)
        node_sens(node,17) = node - 2;
    end
    if(x+2<=num_nodes_x)
        node_sens(node,18) = node + 2;
    end
    if( y+1<=num_nodes_y )
        if(x-2>0)
            node_sens(node,19) = node + num_nodes_x - 2;
        end
        if(x+2<=num_nodes_x)
            node_sens(node,20) = node + num_nodes_x + 2;
        end
    end
    if( y+2<=num_nodes_y )
        if(x-2>0)
            node_sens(node,21) = node + num_nodes_x*2 - 2;
        end
        if(x-1>0)
            node_sens(node,22) = node + num_nodes_x*2 - 1;
        end
        node_sens(node,23) = node + num_nodes_x*2;
        if(x+1<=num_nodes_x)
            node_sens(node,24) = node + num_nodes_x*2 + 1;
        end
        if(x+2<=num_nodes_x)
            node_sens(node,25) = node + num_nodes_x*2 + 2;
        end
    end
        
end

end

