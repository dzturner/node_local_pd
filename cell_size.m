%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [ cell_size ] = cell_size( num_nodes_x, num_nodes_y, h )

cell_size = zeros(num_nodes_x*num_nodes_y,1);

%-----------------------------------------------------
% 
% use this cell size if you want the boundary
% size to be the length specified in the input + h/2
%
% the resulting error in the reaction force will
% be slightly lower than the option below
%
%-----------------------------------------------------

%cell_size(:) = h^2; return;

%-----------------------------------------------------
% 
% use this cell size if you want the boundary
% size to be the length specified in the input
%
% the reaction force error will be slightly
% higher, but you don't have to recompute the
% surface area to check the reaction for
% decreasing h
%
%-----------------------------------------------------

node_gid = 1;
for i=1:num_nodes_y
    for j=1:num_nodes_x
        elem_size = h*h;
        if (j==1||j==num_nodes_x) && (i==1||i==num_nodes_y)
            elem_size = elem_size/4.0;
        else 
          if j==1 || j==num_nodes_x || i==1 || i==num_nodes_y
              elem_size = elem_size/2.0;
          end
        end
        cell_size(node_gid) = elem_size;
        node_gid = node_gid+1;
    end
end

end

