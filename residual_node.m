%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [ this_node_residual ] = residual_node(node,dim,node_stencil,...
    initial_coords, current_coords, cell_size, weighted_size, dilat,...
    K, mu)

this_node_residual = 0.0;
num_entries = length(node_stencil(1,:));

% for 2D K should be stiffer:
K_tilda = K + mu/3.0;

for bond_it=1:num_entries
    if(node_stencil(node,bond_it)==0); continue; end
    left_node  = node;
    right_node = node_stencil(node,bond_it);

    dx = initial_coords(right_node,1) - initial_coords(left_node,1);
    dy = initial_coords(right_node,2) - initial_coords(left_node,2);
    DX = current_coords(right_node,1) - current_coords(left_node,1);
    DY = current_coords(right_node,2) - current_coords(left_node,2);
    x_bar = sqrt(dx^2 + dy^2);
    y_bar = sqrt(DX^2 + DY^2);
    theta_l = dilat(left_node);
    theta_r = dilat(right_node);
    s = cell_size(right_node) * cell_size(left_node);
    m_r = weighted_size(right_node);
    m_l = weighted_size(left_node);
    bond_vec = [DX/y_bar DY/y_bar]*s;
    
    kappa_l = 2.0*K_tilda/m_l;
    kappa_r = 2.0*K_tilda/m_r;
    
    alpha_l = 6*mu/m_l;
    alpha_r = 6*mu/m_r;
    
    value_l = kappa_l * theta_l*x_bar ...
        + alpha_l * ((y_bar-x_bar)-theta_l*x_bar/2.0);
    value_r = kappa_r * theta_r*x_bar ...
        + alpha_r * ((y_bar-x_bar)-theta_r*x_bar/2.0);
        
    this_node_residual = this_node_residual ...
        + value_l*bond_vec(dim) ...
        + value_r*bond_vec(dim);
end
    



end

