%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [ dilat dilat_dual ] = dilat_dual(num_nodes, num_elem, initial_coords, ...
    current_coords, connectivity, h, local_bonds)

dilat           = zeros(num_nodes,1);
num_dilat       = zeros(num_nodes,1);
dilat_dual      = zeros(num_elem,1);
num_local_bonds = length(local_bonds(:,1));

% FIXME ...
m = 8*h^4;
s = h^2; % s is h^2 instead of h^4 since its not a density quantity like force

for i=1:num_elem
    
    for bond = 1:num_local_bonds
    
        left_node = connectivity(i,local_bonds(bond,1));
        right_node = connectivity(i,local_bonds(bond,2));
        dx = initial_coords(right_node,1) - initial_coords(left_node,1);
        dy = initial_coords(right_node,2) - initial_coords(left_node,2);
        DX = current_coords(right_node,1) - current_coords(left_node,1);
        DY = current_coords(right_node,2) - current_coords(left_node,2);
        x_bar = sqrt(dx^2 + dy^2);
        y_bar = sqrt(DX^2 + DY^2);
        
        %e = (y_bar - x_bar)
        
        dilat_dual(i) = dilat_dual(i) ...
            + 2.0 * (y_bar - x_bar) * x_bar * s / m;
                 
    end
    
    % add the element value to all nodes (later it will be averaged)
    for j = 1:length(connectivity(i,:))
        node = connectivity(i,j);
        dilat(node) = dilat(node) + dilat_dual(i);
        num_dilat(node) = num_dilat(node) + 1;
    end
end

dilat = dilat ./ num_dilat;

end

