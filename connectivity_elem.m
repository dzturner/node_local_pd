%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

%   local node ordering:
%
%   4----3
%   |    |
%   1----2
%

function [ elem_connectivity ] = connectivity_elem(num_nodes_x, num_nodes_y)

num_elem = (num_nodes_x - 1)*(num_nodes_y -1);

elem_connectivity = zeros(num_elem,8);

elem_gid = 1;
for i=1:num_nodes_y - 1
    for j=1:num_nodes_x - 1
        if(i>1)
            elem_connectivity(elem_gid,2) = elem_gid - (num_nodes_x-1);
            if(j>1)
                elem_connectivity(elem_gid,1) = elem_gid - (num_nodes_x-1)  - 1;
            end
            if(j<num_nodes_x - 1)
                elem_connectivity(elem_gid,3) = elem_gid - (num_nodes_x-1) + 1;
            end
        end
        if(j>1)
            elem_connectivity(elem_gid,4) = elem_gid - 1;
        end
        if(j<num_nodes_x - 1)
            elem_connectivity(elem_gid,5) = elem_gid + 1;
        end
        if(i<num_nodes_y - 1)
            elem_connectivity(elem_gid,7) = elem_gid + (num_nodes_x-1);
            if(j>1)
                elem_connectivity(elem_gid,6) = elem_gid + (num_nodes_x-1)  - 1;
            end
            if(j<num_nodes_x - 1)
                elem_connectivity(elem_gid,8) = elem_gid + (num_nodes_x-1) + 1;
            end
        end
        elem_gid = elem_gid + 1;
    end
end

end

