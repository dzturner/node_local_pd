%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.
%
%  DESCRIPTION:
%
% Node local peridynamics is a simple 2D peridynamics code
% for regular affine grids that uses the standard FEM stencil
% as the horizon. This local restriction of the horizon leads
% to a formulation equivalent to classical solid mechanics, but
% is integral based and naturally incorporates discontinuities.

% FIXME: clean up the plotting on both
%        try more realistic problems for both

clear; clc;

disp('...                                    ...');
disp('... Dual-mesh-based local peridynamics ...');
disp('...                                    ...');

E  = 100.00E3; % How's it putting in E and nu for a change, Callie?:)
nu = 0.499;

% converted material properties:
K  = E / (3*(1-2*nu));
mu = E / (2*(1+nu));

% number of iterations
max_its = 50;
tol = 1E-3;
epsilon = 1E-3;

% square domain
length = 10.0;
% length must be divisable by h
h = length / 40.0;

num_elem_x = length / h;
num_elem_y = length / h;
num_elem = num_elem_x * num_elem_y;
num_nodes_x = num_elem_x + 1;
num_nodes_y = num_elem_y + 1;
num_nodes = num_nodes_x * num_nodes_y;

% define the element:
local_bonds     = [ 1 2; 2 3; 3 4; 4 1; 1 3; 4 2];

% create the sensetivity stencils:
[ node_stencil, node_sens ] = node_stencil(num_nodes_x, num_nodes_y);

% create the node grid coords
initial_coords = coords(num_nodes_x, num_nodes_y, h);

% create the connectivity list
connectivity = connectivity(num_nodes_x, num_nodes_y);

% create the connectivity of neighboring elements
elem_connectivity = connectivity_elem(num_nodes_x, num_nodes_y);

% initialize displacement field
displacement = zeros(num_nodes,2);

% allocate the tangent matrix
it_tangent = zeros(num_nodes*2,num_nodes*2);

% allocate the reaction vector
internal_force = zeros(num_nodes*2,1);

for iter = 1:max_its+1 

    % boundary conditions
    [ displacement, bc_flags ] = boundary_conditions('lid_driven_cavity', ...
         initial_coords, displacement, num_nodes_x, num_nodes_y);
    %displacement
     
    current_coords = initial_coords + displacement;

    % compute the dilataion
    [ it_dilat, it_dilat_dual ] =  dilat_dual(num_nodes, num_elem, initial_coords, ...
        current_coords, connectivity, h, local_bonds);
    %it_dilat
    
    %compute the residual
    it_residual  = residual_dual(K, mu, h, num_nodes, num_elem, ...
        local_bonds, connectivity, it_dilat_dual, initial_coords, current_coords);
    
    %compute the residual
    %stab_residual  = stab_residual_dual(K, mu, h, num_nodes, num_elem, ...
    %    local_bonds, connectivity, it_dilat, it_dilat_dual, initial_coords, current_coords);
    %norm_resid_stab = norm(stab_residual);
    
    %it_residual = it_residual + stab_residual;

    internal_force = it_residual;
    
    % apply bcs to residual    
    for i=1:num_nodes*2
        if(bc_flags(i)==1)
            it_residual(i)  = 0.0;
        end
    end
    %it_residual    
    
    % test the residual norm
    fprintf('\nIt: %i Residual: %f\n\n', iter,norm(it_residual))
    if(norm(it_residual) < tol)
        fprintf('\n-----------------\n Step Converged!\n-----------------\n'); 
        break;
    end
    if(iter==max_its+1)
        fprintf('\n-----------------\n Step Failed!\n-----------------\n');
        %error();
    end

    % only compute the tangent on the first iteration
    if (iter==1)
         fprintf('STATUS: Computing the tangent...\n')
         
         % compute the dual tangent:
         it_tangent =  tangent_dual(K, mu, h, epsilon, num_nodes, num_elem, ...
             local_bonds, connectivity, initial_coords, current_coords);
         
         norm_reg = norm(it_tangent)
         
         fprintf('STATUS: Done computing the tangent\n')
          
         fprintf('STATUS: Computing the stabilized tangent...\n')
         
         % compute the stabilization tangent:
         %it_stab_tangent = stab_tangent_dual(K, mu, h, epsilon,...
         %    num_nodes, num_elem, ...
         %    local_bonds, connectivity, it_dilat_dual, initial_coords, current_coords,...
         %    elem_connectivity);
         %norm_stab = norm(it_stab_tangent)
         
         %it_tangent = it_tangent + it_stab_tangent;
         
         fprintf('STATUS: Done computing the stabilized tangent\n')

          % apply bcs to the tangent
          for i=1:num_nodes*2
              if(bc_flags(i)==1)
                  it_tangent(i,:) = 0;
                  it_tangent(:,i) = 0;
                  it_tangent(i,i) = 1.0/epsilon;
                  % multiply by the stiffness to scale better
              end
          end
          %it_tangent          
    end
%     
    fprintf('STATUS: Solving...\n')
    lhs = it_tangent \ -it_residual;
    fprintf('STATUS: Done solving\n')
    
    % update the displacments:
    % % update coords
    for node=1:num_nodes
        for dim=1:2
            index = (node-1)*2 + dim;
            displacement(node,dim) = displacement(node,dim) + lhs(index);
        end
    end
     
end

%displacement

disp(' ');
disp('Force reaction at boundary:');
total_reaction = reaction(2,num_nodes_x, num_nodes_y, internal_force)

% compute the analytical reactions
e_yy = 0.0002;
e_xx = 0.0002;
gamma_xy = 0.0;
[ sigma_xx, sigma_yy, tau_xy ] = plane_strain_hookes( ...
    K, mu, e_xx, e_yy, gamma_xy);

force_analytic    = sigma_yy*length
%force_analytic    = tau_xy*length
force_error       = force_analytic - total_reaction(2)
error_percentatge = force_error / force_analytic * 100.0

strain_energy_density_dual = strain_energy_density_dual(K, mu, h, ...
    num_elem, local_bonds, connectivity, it_dilat_dual, initial_coords, current_coords);

%plot_quantity_annotated( internal_force, num_elem, initial_coords, current_coords, connectivity )

%plot_quantity_annotated(internal_force, initial_coords, current_coords) 
%plot_quantity(internal_force,5,length,length,num_nodes_x,num_nodes_y,h); 
%plot_quantity(displacement,5,length,length,num_nodes_x,num_nodes_y,h);
%plot_quantity(strain_energy_density_dual,0,length,length,num_nodes_x,num_nodes_y,h);
plot_quantity(it_dilat_dual,0,length,length,num_nodes_x,num_nodes_y,h);

% plot dilat across top
%px = [h/2:h:length-h/2];
%pd = zeros(num_elem_x,1);
%for i=1:num_elem_x
%    pd(i) = it_dilat_dual((num_elem_y-1)*num_elem_x + i);
%end
%hold on
%plot(px,pd)

