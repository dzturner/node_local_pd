%
%  Copyright 2014 Dan Turner. All rights reserved.
% 
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions 
%  are met:
%
% -- Redistributions of source code must retain the above copyright 
%    notice, this list of conditions and the following disclaimer.
% -- Redistributions in binary form must reproduce the above copyright 
%    notice, this list of conditions and the following disclaimer in the 
%    documentation and/or other materials provided with the distribution.
%
%  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED 
%  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
%  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
%  IN NO EVENT SHALL DAN TURNER BE LIABLE FOR ANY DIRECT, INDIRECT, 
%  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
%  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
%  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
%  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
%  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
%  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

function [ strain_energy_density ] = strain_energy_density_multigrid(K, mu, h, ...
    num_nodes, num_elem, ...
    local_bonds, connectivity, dilat, initial_coords, current_coords)

strain_energy_density = zeros(num_nodes,1);
num_local_bonds       = length(local_bonds(:,1));

m     = 8*h^4;
s     = h^2;
kappa = K + mu/3.0;
alpha = 8.0*mu/m;

% shear component
for i = 1:num_elem

    for bond = 1:num_local_bonds
    
        left_node = connectivity(i,local_bonds(bond,1));
        right_node = connectivity(i,local_bonds(bond,2));
        dx = initial_coords(right_node,1) - initial_coords(left_node,1);
        dy = initial_coords(right_node,2) - initial_coords(left_node,2);
        DX = current_coords(right_node,1) - current_coords(left_node,1);
        DY = current_coords(right_node,2) - current_coords(left_node,2);
        x_bar = sqrt(dx^2 + dy^2);
        y_bar = sqrt(DX^2 + DY^2);

        theta_left  = dilat(left_node);
        theta_right = dilat(right_node);
        
        e_d_left  = ((y_bar-x_bar)-theta_left*x_bar/2.0);
        e_d_right = ((y_bar-x_bar)-theta_right*x_bar/2.0);
        
        % shear component
        value_left  = 0.5 * alpha*e_d_left*e_d_left*s;
        value_right = 0.5 * alpha*e_d_right*e_d_right*s;
       
        strain_energy_density(left_node)   = strain_energy_density(left_node)  + value_left;
        strain_energy_density(right_node)  = strain_energy_density(right_node) + value_right;

    end
    
end

% add the dilatational component
for i = 1:num_nodes
    strain_energy_density(i) = strain_energy_density(i) + 0.5 * kappa * dilat(i) * dilat(i);
end


end

